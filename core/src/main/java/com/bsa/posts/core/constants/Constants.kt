package com.bsa.posts.core.constants

import com.bsa.posts.core.BuildConfig

object Constants {
    val API_URL = BuildConfig.BASE_URL

    object Posts {
        val DB_NAME = "posts_db"
    }
}