package com.bsa.posts.details.exceptions

interface DetailsExceptions {
    class NoComments : Exception()
}