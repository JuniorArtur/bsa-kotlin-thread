package com.bsa.posts.list.di

import android.content.Context
import androidx.room.Room
import com.bsa.posts.commons.data.local.PostDb
import com.bsa.posts.commons.data.remote.PostService
import com.bsa.posts.core.constants.Constants
import com.bsa.posts.core.di.CoreComponent
import com.bsa.posts.core.networking.Scheduler
import com.bsa.posts.list.ListActivity
import com.bsa.posts.list.PostListAdapter
import com.bsa.posts.list.model.ListDataContract
import com.bsa.posts.list.model.ListLocalData
import com.bsa.posts.list.model.ListRemoteData
import com.bsa.posts.list.model.ListRepository
import com.bsa.posts.list.viewmodel.ListViewModelFactory
import com.squareup.picasso.Picasso
import dagger.Component
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit

@ListScope
@Component(dependencies = [CoreComponent::class], modules = [ListModule::class])
interface ListComponent {

    fun postDb(): PostDb

    fun postService(): PostService
    fun picasso(): Picasso
    fun scheduler(): Scheduler

    fun inject(listActivity: ListActivity)
}

@Module
class ListModule {

    @Provides
    @ListScope
    fun adapter(picasso: Picasso): PostListAdapter = PostListAdapter(picasso)

    @Provides
    @ListScope
    fun listViewModelFactory(repository: ListDataContract.Repository, compositeDisposable: CompositeDisposable): ListViewModelFactory = ListViewModelFactory(repository, compositeDisposable)

    @Provides
    @ListScope
    fun listRepo(local: ListDataContract.Local, remote: ListDataContract.Remote, scheduler: Scheduler, compositeDisposable: CompositeDisposable): ListDataContract.Repository = ListRepository(local, remote, scheduler, compositeDisposable)

    @Provides
    @ListScope
    fun remoteData(postService: PostService): ListDataContract.Remote = ListRemoteData(postService)

    @Provides
    @ListScope
    fun localData(postDb: PostDb, scheduler: Scheduler): ListDataContract.Local = ListLocalData(postDb, scheduler)

    @Provides
    @ListScope
    fun compositeDisposable(): CompositeDisposable = CompositeDisposable()

    @Provides
    @ListScope
    fun postDb(context: Context): PostDb = Room.databaseBuilder(context, PostDb::class.java, Constants.Posts.DB_NAME).build()

    @Provides
    @ListScope
    fun postService(retrofit: Retrofit): PostService = retrofit.create(PostService::class.java)
}