package com.bsa.posts.core.application

import android.app.Application
import com.bsa.posts.core.BuildConfig
import com.bsa.posts.core.di.AppModule
import com.bsa.posts.core.di.CoreComponent
import com.bsa.posts.core.di.DaggerCoreComponent
import com.bsa.posts.core.networking.synk.Synk
import com.facebook.stetho.Stetho

open class CoreApp : Application() {

    companion object {
        lateinit var coreComponent: CoreComponent
    }

    override fun onCreate() {
        super.onCreate()
        initSynk()
        initDI()
        initStetho()
    }

    private fun initSynk() {
        Synk.init(this)
    }

    private fun initStetho() {
        if (BuildConfig.DEBUG)
            Stetho.initializeWithDefaults(this)
    }

    private fun initDI() {
        coreComponent = DaggerCoreComponent.builder().appModule(AppModule(this)).build()
    }
}