package com.bsa.posts.details.di

import com.bsa.posts.commons.data.local.PostDb
import com.bsa.posts.commons.data.remote.PostService
import com.bsa.posts.core.networking.Scheduler
import com.bsa.posts.details.DetailsActivity
import com.bsa.posts.details.model.DetailsDataContract
import com.bsa.posts.details.model.DetailsLocalData
import com.bsa.posts.details.model.DetailsRemoteData
import com.bsa.posts.details.model.DetailsRepository
import com.bsa.posts.details.viewmodel.DetailsViewModelFactory
import com.bsa.posts.list.di.ListComponent
import dagger.Component
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

@DetailsScope
@Component(dependencies = [ListComponent::class], modules = [DetailsModule::class])
interface DetailsComponent {
    fun inject(detailsActivity: DetailsActivity)
}

@Module
class DetailsModule {

    /*ViewModel*/
    @Provides
    @DetailsScope
    fun detailsViewModelFactory(repo: DetailsDataContract.Repository, compositeDisposable: CompositeDisposable): DetailsViewModelFactory {
        return DetailsViewModelFactory(repo, compositeDisposable)
    }

    /*Repository*/
    @Provides
    @DetailsScope
    fun detailsRepo(local: DetailsDataContract.Local, remote: DetailsDataContract.Remote, scheduler: Scheduler, compositeDisposable: CompositeDisposable)
            : DetailsDataContract.Repository = DetailsRepository(local, remote, scheduler, compositeDisposable)

    @Provides
    @DetailsScope
    fun remoteData(postService: PostService): DetailsDataContract.Remote = DetailsRemoteData(postService)

    @Provides
    @DetailsScope
    fun localData(postDb: PostDb, scheduler: Scheduler): DetailsDataContract.Local = DetailsLocalData(postDb, scheduler)

    @Provides
    @DetailsScope
    fun compositeDisposable(): CompositeDisposable = CompositeDisposable()
}